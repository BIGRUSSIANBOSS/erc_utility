var app = angular.module('ERC', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'MainCtrl',
            templateUrl: 'templates/search.html'
        })
        .when('/params/:id', {
            controller: 'ParamCtrl',
            templateUrl: 'templates/parametr.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});