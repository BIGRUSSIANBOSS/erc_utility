var app = angular.module('ERC', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'searchController',
            templateUrl: 'templates/searcher.html'
        })
        .when('/parameters/:id', {
            controller: 'ParamCtrl',
            templateUrl: 'templates/parameter.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});