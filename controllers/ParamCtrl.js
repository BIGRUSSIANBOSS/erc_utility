
// javascript

//angular


app.controller('ParamCtrl', ['$scope','$http','$location','$routeParams','$route','$sce','$timeout', function($scope, $http, $location,$routeParams,$route,$sce,$timeout) {
    //console.log($route.current.params.id);

    // variables

    var notTimerId = 0;

    $scope.current_position = 0;

    $scope.saving_status = 0;

    $scope.hotice = [false,false];

    $scope.form = [];

    $scope.customProperties = [];

    // public functions

    $scope.getIndications = function (row) {

        $scope.parameters = {
            'current': '',
            'prev': ''
        };

        if(row.indications.indication[0].periodIsOpen){  // пришли оба параметра

            $scope.parameters.current = row.indications.indication[0];
            $scope.parameters.prev = row.indications.indication[1];

        }else if(!row.indications.indication[0].periodIsOpen){ // пришла одна индикация

            $scope.parameters.prev = row.indications.indication[1];

        }
    };


    $scope.saveParameters = function(){

        var options = [];

        for(id in $scope.form){
            if($scope.form[id].length > 0){
                options.push({"Id":id,"CurrentValue":$scope.form[id]});
            }
        }

        $data_string = {"Row":
            options
        };
        $scope.saving_status = 1;

        $http.post('http://195.211.40.26:8448/ds-webapp/rest/indication/collect',$data_string).
            success(function(data, status, headers, config) {
                if(status == 204){
                    $scope.saving_status = 0;
                }else{
                    $scope.saving_status = 2;
                }
                //$route.reload();
            }).
            error(function(data, status, headers, config) {

            });

    };

    $scope.onFillParams = function(e){

        switch(e.keyCode){
            case 13:
            case 40: // вниз

                if(e.ctrlKey){
                    $scope.saveParameters();
                    $scope.notify('Сохранено, нажмите <b>Esc</b> чтобы перейти к поиску или продолжите наполнение с помощь клавиш вверх/вниз',10000);

                    $location.path( "/");

                    //$timeout(function(){
                    //    document.getElementsByClassName('erc_search')[0].select();
                    //});
                }else{

                    var selectable = document.getElementsByClassName('selectable');

                    for(selectedItem in selectable){
                        if(document.activeElement == selectable[selectedItem]){
                            selectedItem++;
                            if(selectedItem > selectable.length-1){
                                selectedItem = 0;
                            }
                            selectable[selectedItem].focus();
                            $timeout(function(){
                                selectable[selectedItem].select();
                            });
                            break;
                        }
                    }

                }

                break;
            case 38: // вверх

                var selectable = document.getElementsByClassName('selectable');

                for(selectedItem in selectable){
                    if(document.activeElement == selectable[selectedItem]){
                        selectedItem--;
                        if(selectedItem < 0){
                            selectedItem = selectable.length-1;
                        }
                        selectable[selectedItem].focus();
                        $timeout(function(){
                            selectable[selectedItem].select();
                        });
                        break;
                    }
                }

                break;
            case 27: // esc

                $location.path( "/");
                //var erc_search = document.getElementsByClassName('erc_search');
                //erc_search[0].focus();
                //erc_search[0].select();
                //$scope.notify('Введите адрес в формате улица дом-квартира<br>пример: Николая Островского 113-54 или введите номер личного счета пример: 3060584 <br><br> Для справки нажмите Ctrl+H',10000);
                break;
            case 0: // enter
                //$scope.saveParameters();
                //$scope.notify('Сохранено, нажмите <b>Esc</b> чтобы перейти к поиску или продолжите наполнение с помощь клавиш вверх/вниз',10000);
                ////$timeout(function(){
                ////    document.getElementsByClassName('erc_search')[0].select();
                ////});
                //break;
            default:
                    console.log(e.which);
                break;
        }
    };

    $scope.notify = function (text, time) {
            clearTimeout(notTimerId);
            var notification = document.getElementById('ui-notification');
            notification.innerHTML = text;
            notification.className = 'active';
            notTimerId = setTimeout(removeClass,time);
            function removeClass(){
                notification.className = '';
            }
    };

    $scope.getData = function () {

        $data_string = {'GetMeterData':{'name':$route.current.params.id}};

        $http.post('http://195.211.40.26:8448/ds-webapp/rest/req',$data_string).
            success(function(data, status, headers, config) {
                if(data.String != 'no response'){
                    if(data.GetMetersResp.accounts.account.length > 0){

                        $scope.meters = data.GetMetersResp.meters.meter;
                        $scope.account = data.GetMetersResp.accounts.account[0];
                        $scope.address = data.GetMetersResp.address;
                        //if(data.GetMetersResp.meters.length > 0){
                        //console.log(data.hasOwnProperty('GetMetersResp'));


                        for(meter in $scope.meters){
                            if($scope.meters[meter].customProps.customProp.length != 0){
                                var props = $scope.meters[meter].customProps.customProp;
                                for(prop in props){
                                    $scope.customProperties.push({"name":props[prop].propName, "id":prop});
                                }
                            }
                        }

                        console.log($scope.customProperties);

                        if($scope.meters != null){
                            //$scope.getIndication($scope.meters[$scope.current_position]);
                            $timeout(function(){
                                var selectable = document.getElementsByClassName('selectable');
                                selectable[0].select();
                            });
                        }
                        //}

                        //console.log(data.GetMetersResp.meters.meter);
                    }else{
                        $scope.account = null;
                    }

                }else{
                    $scope.account = null;
                }
            }).
            error(function(data, status, headers, config) {

            });
    };

    $scope.checkHotOrNot = function (_class) {


            if($scope.meter.supplyType == "Горячая вода"){
                return $scope.hotice[0];
            }else{
                return $scope.hotice[1];
            }


        //console.log(_class);

    };

    $scope.checkSupply = function () {

            if (this.meter.supplyType == "Горячая вода") {
                $scope.hotice[1] = !$scope.hotice[1];
            } else {
                $scope.hotice[0] = !$scope.hotice[0];
            }

    };

    $scope.showSupply = function () {

        try{
            if (this.meter.supplyType == "Горячая вода") {
                $scope.supplyType = $sce.trustAsHtml("&#xe806;");
                $scope.supplyClass = "icon-temperature";
                $scope.supplyBg = "orange";
                $scope.supplyHot = true;
                $scope.supplyIce = false;
            } else {
                $scope.supplyType = $sce.trustAsHtml("&#xe805;");
                $scope.supplyClass = "icon-snow";
                $scope.supplyBg = "blue";
                $scope.supplyHot = false;
                $scope.supplyIce = true;
            }
        }catch (err){
            console.log('this is an error! Try to check it out');
        }
    };

    $scope.previous = function () {

        if($scope.current_position > 0){
            $scope.current_position--;
        }else{
            $scope.current_position = $scope.meters.length-1;
        }

        $scope.getIndication($scope.meters[$scope.current_position]);
    };

    $scope.next = function () {

        if($scope.current_position == $scope.meters.length-1){
            $scope.current_position = 0;
        }else{
            $scope.current_position++;
        }

        $scope.getIndication($scope.meters[$scope.current_position]);
        //console.log($scope.meters[$scope.current_position]);
    };

    $scope.getIndication = function (meter) {
        $scope.showSupply();
        $scope.indication = meter.rows.row[0].indications.indication;
    };

}]).directive('numbersOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {

                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});