app.controller('MainCtrl', ['$scope','$http','$location','asyncService', function($scope, $http, $location,asyncService) {

    // Variables

    var notifyTimerId = 0;

    $scope.active_item_index = 0;

    var selectable = document.getElementsByClassName('erc_search');
    selectable[0].select();

    // public functions

    $scope.found = function(search){  // searching results

        if(typeof search != "undefined"){
            if (search.length > 0) {

                $scope.itemsList = [];
                $data_string = {'GetGeoData':{'name':search}};

                $http.post('http://195.211.40.26:8448/ds-webapp/rest/req',$data_string).
                    success(function(data, status, headers, config) {

                        $scope.extendedSearch($scope, asyncService,$data_string);

                        return true;
                    }).
                    error(function(data, status, headers, config) {
                        //console.log(status);
                        return false;
                    });

            }else{
                $scope.items = [];
                return false;
            }
        }else{
            $scope.items = [];
            return false;
        }
    };

    $scope.extendedSearch = function($scope, asyncService,$data_string) { // callback for results
        var myDataPromise = asyncService.getData($data_string);
        myDataPromise.then(function(result) {  // this is only run after $http completes
            if(result.hasOwnProperty('GetGeoDataResp')){
                $scope.items = result.GetGeoDataResp.items;
            }else{
                $scope.items = [];
            }
            //console.log(data);
        });
    };

    $scope.notify = function (text, time) {
        clearTimeout(notifyTimerId);
        var notification = document.getElementById('ui-notification');
        notification.innerHTML = text;
        notification.className = 'active';
        notifyTimerId = setTimeout(removeClass,time);
        function removeClass(){
            notification.className = '';
        }
    };

    $scope.onKeydown = function(e){

        //console.log($scope.items);

        var loopArray = function (array, stop, reverse){

            reverse = reverse || false;

            for(item in array){
                if(item == stop){
                    if(!reverse){
                        stop++;
                        if(stop > array.length-1){
                            stop = 0;
                        }
                    }else{
                        stop--;
                        if(stop < 0){
                            stop = array.length-1;
                        }
                    }
                    return stop;
                }
            }
        };

        switch(e.which){
            case 40: // вверх

                $scope.active_item_index = loopArray($scope.items, $scope.active_item_index);


                break;
            case 38: // вниз

                $scope.active_item_index = loopArray($scope.items, $scope.active_item_index, true);

                break;
            case 13: // вниз

                try{
                    $scope.setSearch($scope.items[$scope.active_item_index]);
                    $scope.found($scope.search);
                    $scope.checkAvailable($scope.items[$scope.active_item_index]);
                    $scope.active_item_index = 0;
                }catch (err){
                    $scope.active_item_index = 0;
                    console.log('another problem');
                }

                break;
            default:

                //console.log($scope);
                $scope.found($scope.search);
                $scope.active_item_index = 0;
                break;
        }

    };

    $scope.setSearch = function(item){


        if(item.house != null){

            $scope.search = item.street.formalName + ' ' + item.house.houseNumber + '-';
            if(item.flat != null){
                $scope.search = item.street.formalName + ' ' + item.house.houseNumber + '-' + item.flat.number;
            }

        }else{

            $scope.search = item.street.formalName;

        }

        //console.log($scope);

        $scope.found($scope.search);
        $scope.checkAvailable(item);

    };

    $scope.openItem = function(item){
        //console.log(item);
        if(item != null){
            $location.path( "/params/" + item);
        }
    };

    $scope.checkAvailable = function(item){

        var success = function(data, status, headers, config){
            if(data.String != 'no response'){
                if(data.GetMetersResp.accounts.account.length > 0){
                    $scope.selectedItem = data.GetMetersResp.accounts.account[0].accountNumber;
                    $scope.openItem(data.GetMetersResp.accounts.account[0].accountNumber);
                }else{
                    $scope.selectedItem = null;
                }
            }else{
                $scope.selectedItem = null;
            }
        };

        // checking search response

        var id = item.street.formalName.split(' '); // get first word

        if(Number.isInteger(Number(id[0]))){ // if has account number

            id = id[0];
            $data_string = {'GetMeterData':{'name':id}};
            $http.post('http://195.211.40.26:8448/ds-webapp/rest/req',$data_string).
                success(success);

        }else{ // if write a street

            var response = item.street.formalName+' ' + item.house.houseNumber + '-' +item.flat.number;
            $data_string = {'GetMeterData':{'name':response}};
            $http.post('http://195.211.40.26:8448/ds-webapp/rest/req',$data_string).
                success(success);

        }

    };


}]).factory('asyncService', function($http) {

    var getData = function($data_string) {


        return $http.post('http://195.211.40.26:8448/ds-webapp/rest/req',$data_string).then(function(result){
            return result.data;
        });


    };
    return { getData: getData };

});